// --------------------------------------------------------------------------
// gMini,
// a minimal Glut/OpenGL app to extend                              
//
// Copyright(C) 2007-2009                
// Tamy Boubekeur
//                                                                            
// All rights reserved.                                                       
//                                                                            
// This program is free software; you can redistribute it and/or modify       
// it under the terms of the GNU General Public License as published by       
// the Free Software Foundation; either version 2 of the License, or          
// (at your option) any later version.                                        
//                                                                            
// This program is distributed in the hope that it will be useful,            
// but WITHOUT ANY WARRANTY; without even the implied warranty of             
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
// GNU General Public License (http://www.gnu.org/licenses/gpl.txt)           
// for more details.                                                          
//                                                                          
// --------------------------------------------------------------------------
/*float hash(float p) { p = fract(p * 0.011); p *= p + 7.5; p *= p + p; return fract(p); }
float hash(vec2 p) {vec3 p3 = fract(vec3(p.x, p.y, p.x) * 0.13); p3 += dot(p3, p3.yzx + 3.333); return fract((p3.x + p3.y) * p3.z); }

float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

float noise(float x) {
    float i = floor(x);
    float f = fract(x);
    float u = f * f * (3.0 - 2.0 * f);
    return min(hash(i), min(hash(i + 1.0), u));
}

float noise2 (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);
 
    // Four corners in 2D of a tile
    float a = random(i);
    /*float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));
 
    //vec2 u = f * f * (3.0 - 2.0 * f);
 
    return 0; 
}

float noise3(vec3 x) {
    const vec3 step = vec3(110, 241, 171);
 
    vec3 i = floor(x);
    vec3 f = fract(x);
 
    // For performance, compute the base input to a 1D hash from the integer part of the argument and the
    // incremental change to the 1D based on the 3D -> 1D wrapping
    float n = dot(i, step);
 
    vec3 u = f * f * (3.0 - 2.0 * f);
    return min(min(min( hash(n + dot(step, vec3(0, 0, 0))), hash(n + dot(step, vec3(1, 0, 0))), u.x),
                   min( hash(n + dot(step, vec3(0, 1, 0))), hash(n + dot(step, vec3(1, 1, 0))), u.x), u.y),
               min(min( hash(n + dot(step, vec3(0, 0, 1))), hash(n + dot(step, vec3(1, 0, 1))), u.x),
                   min( hash(n + dot(step, vec3(0, 1, 1))), hash(n + dot(step, vec3(1, 1, 1))), u.x), u.y), u.z);
}


 
float fbm (in vec2 st) {
    // Initial values
    float value = 0.0;
    float amplitud = 0.5;
    //float frequency = 0.;
    //
    // Loop of octaves
    for (int i = 0; i < 6; i++) {
        value += amplitud * noise(st);
        st *= 2.0;
        amplitud *= 0.5;
    }
    return value;
}
float fbm3 (in vec3 st) {
    // Initial values
    float value = 0.0;
    float amplitud = 0.5;
    //float frequency = 0.;
    //
    // Loop of octaves
    for (int i = 0; i < 6; i++) {
        value += amplitud * noise3(st);
        st *= 2.0;
        amplitud *= 0.5;
    }
    return value;
}
 
vec2 fbmToVec2(in vec2 st) {
    return vec2(fbm(st), fbm(st + 100.0));
}
vec2 fbm3ToVec2(in vec3 st) {
    return vec2(fbm3(st), fbm3(st + 100.0));
}
vec3 fbmToVec3(in vec2 st) {
    return vec3(fbm(st), fbm(st + 18.0), fbm(st - 10.0));
}
vec3 fbm3ToVec3(in vec3 st) {
    return vec3(fbm3(st), fbm3(st + 18.0), fbm3(st - 10.0));
}
*/
uniform float ambientRef;
uniform float diffuseRef;
uniform float specRef;
uniform float shininess;

varying vec4 p;
varying vec3 n;

vec4 toon(float diffuse);
void main (void) {
    vec3 P = vec3 (gl_ModelViewMatrix * p); //Position du point clairer
    vec3 N = normalize (gl_NormalMatrix * n); //Normal en ce point
    vec3 V = normalize (-P); //Vecteur de vue

    vec4 contribution;
    vec4 lightContribution = ambientRef * gl_LightModel.ambient*gl_FrontMaterial.ambient ;
   for(int i = 0; i < 3; i++){
        vec3 L = normalize (gl_LightSource[i].position.xyz);
        float diffuse = dot(N,L);
        contribution = toon(diffuse);
        vec3 R = reflect(L,N);
        float spec = max(dot(R, V), 0.0);
        spec = pow(spec, shininess);
        
       lightContribution += diffuseRef * gl_LightSource[i].diffuse *diffuse* gl_FrontMaterial.diffuse;
       lightContribution += specRef *spec* gl_LightSource[i].specular * gl_FrontMaterial.specular;
    }
	//lightContribution += contribution;


    gl_FragColor =vec4 (lightContribution.xyz, 1);
}

vec4 toon(float diffuse){
    vec4 contribution;
    if(diffuse > 0.90){
        contribution = vec4(1.0,0.5,0.5,1.0);
    }
    else if(diffuse > 0.5){
        contribution = vec4(0.6,0.3,0.3,1.0);
    }
    else if(diffuse > 0.25){
        contribution = vec4(0.4,0.2,0.2,1.0);
    }
    else{
        contribution = vec4(0.2,0.1,0.1,1.0);
    }

    return contribution;
}

 
